---
title: Java with Bitbucket Pipelines
platform: identity
product: identity
category: devguide
subcategory: pipelines
date: "2017-10-11"
---


This guide describes how to use Bitbucket Pipelines for building and testing a Java software project in a Docker container, using either Maven or Gradle as your build tool.

Most of the configuration happens in the `bitbucket-pipelines.yml` file that Pipelines uses to define the build.

# Build and test Maven projects

## Specify your Maven version with Docker

Bitbucket Pipelines runs all your builds in Docker containers using an image that you specify at the beginning of your configuration file. You can easily use Maven with Bitbucket Pipelines by using one of the official Maven Docker images on Docker Hub.

For instance, you can use Maven 3.3.9 (current version as of writing) by specifying it at the beginning of your `bitbucket-pipelines.yml` file.

```yaml
image: maven:3.3.9
pipelines:
  default:
    - step:
        script:
          - mvn -version
```

You can check your `bitbucket-pipelines.yml` file with our online validator.

## Java and JDK versions
By default, the Maven image will include a recent official version of the Java JDK, but you can specify a different version instead by using a specific Maven version. Here's an example using the Docker image for Maven 3.2.5 with JDK 1.7.
```yaml
image: maven:3.2.5-jdk-7
```
You can find a list of available Maven/JDK versions and corresponding image tags, refer to https://hub.docker.com/r/library/maven/.

## Build and run tests with Maven
Once you have selected your Docker image, running tests is just including the appropriate Maven commands in your Pipelines script. Here's a simple example.
```yaml
image: maven:3.3.9
pipelines:
  default:
    - step:
        script:
          - mvn -B verify    # -B batch mode makes Maven less verbose
```
## Private Maven repositories
To access a private Maven repository, you'll need to override the default Maven settings.xml found in the Docker image at `/usr/share/maven/conf/settings.xml`.

Before using this example, you should configure secure environment variables for MAVEN_USERNAME and MAVEN_PASSWORD to pass these values through securely to the Pipelines configuration.

```yaml
image: maven:3.3.9

pipelines:
  branches:
    master:
      - step:
          script:
            - bash configure-maven.sh
            - mvn -B verify
```
Here's `configure-maven.sh`:

```bash
#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>private-repo</id>\
  <username>${MAVEN_USERNAME}</username>\
  <password>${MAVEN_PASSWORD}</password>\
</server>" /usr/share/maven/conf/settings.xml

sed -i "/<profiles>/ a\
<profile>\
  <id>private-repo</id>\
  <activation>\
    <activeByDefault>true</activeByDefault>\
  </activation>\
  <repositories>\
    <repository>\
      <id>private-repo</id>\
      <url>https://example.com/path/to/maven/repo/</url>\
    </repository>\
  </repositories>\
  <pluginRepositories>\
    <pluginRepository>\
      <id>private-repo</id>\
      <url>https://example.com/path/to/maven/repo/</url>\
    </pluginRepository>\
  </pluginRepositories>\
</profile>" /usr/share/maven/conf/settings.xml
```

## Custom Maven settings.xml
Making more advanced changes to the default settings.xml with sed can be quite painful, so it may be easier to commit your own settings.xml to your Bitbucket repository. Then you can reference it using Maven's -s flag.
```yaml
image: maven:3.3.9
pipelines:
  default:
    - step:
        script:
          - mvn -s settings.xml   # e.g. with settings.xml in the root of the project
```
## Build and test Gradle projects
Add the Gradle Wrapper to your repository
Our recommended approach (and Gradle's) to building Gradle projects is to commit the Gradle Wrapper to your repository.

The Gradle wrapper ensures two things:

* the build environment doesn't need to have Gradle manually installed to build your project
* your project is always built with the same Gradle version

We recommend making the wrapper script executable before committing it to your repository:

`chmod +x gradlew`

Then you can build your Gradle project with the wrapper:

```yaml
image: java:8
pipelines:
  default:
    - step:
        script:
          - bash ./gradlew build
```

Remember, you can check your `bitbucket-pipelines.yml` file with our online validator.
